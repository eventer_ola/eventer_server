package common.controllers;

/**
 * Created by rg on 05-Sep-15.
 */

import common.dao.EventDAO;
import common.dao.PersonDAO;
import common.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApis {

    @Autowired
    private PersonDAO personDao;

    @Autowired
    private EventDAO eventDao;

    @RequestMapping(value = "/addEvent", method = RequestMethod.GET)
    public Event addEvent(@RequestParam(value="eventName", required=true) String eventName,
                              @RequestParam(value="eventLocation", required=true) String eventLocation,
                              Model model) {

        Event event = new Event(eventName,eventLocation);

        eventDao.saveOrUpdate(event);
        return event;
    }





}
