package common.controllers;

/**
 * Created by rg on 04-Sep-15.
 */
import common.dao.EventDAO;
import common.dao.PersonDAO;
import common.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {

    @Autowired
    private PersonDAO personDao;



    @Autowired
    private EventDAO eventDAO;

    @RequestMapping(value = "/homePage", method = RequestMethod.GET)
    public String homePage(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }

    @RequestMapping(value = "/getRsvp", method = RequestMethod.GET)
    public String getRsvp(@RequestParam(value="email", required=true) String email,
                                Model model) {
        Person person = personDao.get(email);
        model.addAttribute("person",person);
        return "rsvp";
    }
}
