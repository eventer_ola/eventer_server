package common.dao;

/**
 * Created by rg on 05-Sep-15.
 */

import common.model.Event;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class EventDAOImpl implements EventDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public EventDAOImpl() {
    }

    public EventDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<Event> list() {
        @SuppressWarnings("unchecked")
        List<Event> listEvent = (List<Event>) sessionFactory.getCurrentSession()
                .createCriteria(Event.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return listEvent;
    }

    @Override
    @Transactional
    public void saveOrUpdate(Event event) {
        sessionFactory.getCurrentSession().saveOrUpdate(event);
    }

    @Override
    @Transactional
    public void delete(int eventId) {
        Event eventToDelete = new Event();
        eventToDelete.setEventId(eventId);
        sessionFactory.getCurrentSession().delete(eventToDelete);
    }

    @Override
    @Transactional
    public Event get(int eventId) {
        String hql = "from Event where event_id = :eventId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("eventId", eventId);

        @SuppressWarnings("unchecked")
        List<Event> listEvent = (List<Event>) query.list();

        if (listEvent != null && !listEvent.isEmpty()) {
            return listEvent.get(0);
        }

        return null;
    }
}
