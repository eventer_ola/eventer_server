package common.dao;

/**
 * Created by rg on 04-Sep-15.
 */

import common.model.Person;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PersonDAOImpl implements PersonDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public PersonDAOImpl() {
    }

    public PersonDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public List<Person> list() {
        @SuppressWarnings("unchecked")
        List<Person> listPerson = (List<Person>) sessionFactory.getCurrentSession()
                .createCriteria(Person.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return listPerson;
    }

    @Override
    @Transactional
    public List<Person> listPersonsInEvent(int eventId) {
        @SuppressWarnings("unchecked")
        String hql = "from Person where branch_id = :eventId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("branchId", eventId);

        @SuppressWarnings("unchecked")
        List<Person> listPerson = (List<Person>) query.list();

        if (listPerson != null && !listPerson.isEmpty()) {
            return listPerson;
        }

        return null;
    }

    @Override
    @Transactional
    public void saveOrUpdate(Person person) {
        sessionFactory.getCurrentSession().saveOrUpdate(person);
    }

    @Override
    @Transactional
    public void delete(String email) {
        Person personToDelete = new Person();
        personToDelete.setEmail(email);
        sessionFactory.getCurrentSession().delete(personToDelete);
    }

    @Override
    @Transactional
    public Person get(String email) {
        String hql = "from Person where email = :email";
        Query query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("email", email);

        @SuppressWarnings("unchecked")
        List<Person> listPerson = (List<Person>) query.list();

        if (listPerson != null && !listPerson.isEmpty()) {
            return listPerson.get(0);
        }

        return null;
    }
}
