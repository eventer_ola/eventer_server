package common.dao;

/**
 * Created by rg on 04-Sep-15.
 */
import common.model.Person;

import java.util.List;

public interface PersonDAO {
    public List<Person> list();

    public List<Person> listPersonsInEvent(int branchId);

    public Person get(String mobileNumber);

    public void saveOrUpdate(Person person);

    public void delete(String mobileNumber);
}
