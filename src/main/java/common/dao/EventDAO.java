package common.dao;

import common.model.Event;

import java.util.List;

/**
 * Created by rg on 05-Sep-15.
 */

public interface EventDAO {
    public List<Event> list();

    public Event get(int branchId);

    public void saveOrUpdate(Event event);

    public void delete(int branchId);
}
