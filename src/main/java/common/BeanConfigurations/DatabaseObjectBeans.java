package common.BeanConfigurations;

import common.dao.*;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by rg on 04-Sep-15.
 */
@Configuration
@EnableTransactionManagement
public class DatabaseObjectBeans {

    @Autowired
    @Bean(name = "personDao")
    public PersonDAO getPersonDao(SessionFactory sessionFactory) {
        return new PersonDAOImpl(sessionFactory);
    }

    @Autowired
    @Bean(name = "eventDao")
    public EventDAO getEventDao(SessionFactory sessionFactory) {
        return new EventDAOImpl(sessionFactory);
    }


}
