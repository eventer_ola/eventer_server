package common.model;

/**
 * Created by rg on 05-Sep-15.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "events")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements Serializable { // composite ID must implement Serializable
    @Id
    @GeneratedValue
    @Column(name = "event_id", unique = true)
    private int eventId;

    @Column(name = "eventname")
    private String eventName;

    @Column(name = "eventLocation")
    private String eventLocation;

    @OneToMany(mappedBy = "event")
    @JsonIgnore
    private Set<Person> persons;

    public Event() {
    }

    public Event(String eventName, String eventLocation) {
        this.eventName = eventName;
        this.eventLocation = eventLocation;
    }

    public Event(String eventName, String eventLocation, Set<Person> persons) {
        this.eventName = eventName;
        this.eventLocation = eventLocation;
        this.persons = persons;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }
}
