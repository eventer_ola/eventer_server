package common.model;

/**
 * Created by rg on 04-Sep-15.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "Persons")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {

    @Id
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name = "rsvp")
    private Boolean rsvp;

    @ManyToOne
    @JoinColumn(name = "event_id")
    @JsonIgnore // Highly important because if not specified, during jackson conversion of object to json, it will try to load object using hibernate session and will fail.
    private Event event;

    public Person() {}

    public Person(String email, String name, Boolean rsvp, Event event) {
        this.email = email;
        this.name = name;
        this.rsvp = rsvp;
        this.event = event;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getRsvp() {
        return rsvp;
    }

    public void setRsvp(Boolean rsvp) {
        this.rsvp = rsvp;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
